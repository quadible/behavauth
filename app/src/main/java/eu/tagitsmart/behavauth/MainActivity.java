/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import eu.tagitsmart.behavauthlib.services.SensingService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Intent intent;
    private DataUpdateReceiver dataUpdateReceiver;

    private static boolean hasTrainingCompleted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        disableAuthenticationButtons();

        intent = new Intent(MainActivity.this, SensingService.class);
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register broadcast listener to receive messages from service
        if (dataUpdateReceiver == null) dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(SensingService.INTENT_DATA_SERVICE);
        registerReceiver(dataUpdateReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister broadcast listener to stop receiving messages from service.
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(intent);
    }

    /**
     * This broadcast receiver is used to receive messages from the SensingService
     */
    public class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(SensingService.INTENT_DATA_SERVICE)) {
                enableAllButtons();

                if (intent.getBooleanExtra(SensingService.EXTRA_TRAINING_FINISHED, false)) {
                    hasTrainingCompleted = true;
                    enableAuthenticationButtons();

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.constraint_layout),
                            R.string.msg_training_finished, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } else {
                    hasTrainingCompleted = false;
                    disableAuthenticationButtons();

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.constraint_layout),
                            R.string.msg_training_failed, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        }
    }

    private void initUI() {
        Button startUserTrain = findViewById(R.id.btn_user_start_training);
        startUserTrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_USER_TRAINING);

                disableAllButtons();

                Button stopUserTrain = findViewById(R.id.btn_user_stop_training);
                stopUserTrain.setEnabled(true);
            }
        });

        Button stopUserTrain = findViewById(R.id.btn_user_stop_training);
        stopUserTrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_IDLE);

                enableAllButtons();
                if (!hasTrainingCompleted) disableAuthenticationButtons();
            }
        });

        Button startImposterTrain = findViewById(R.id.btn_imposter_start_training);
        startImposterTrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_IMPOSTER_TRAINING);

                disableAllButtons();

                Button stopImposterTrain = findViewById(R.id.btn_imposter_stop_training);
                stopImposterTrain.setEnabled(true);
            }
        });

        Button stopImposterTrain = findViewById(R.id.btn_imposter_stop_training);
        stopImposterTrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_IDLE);

                enableAllButtons();
                if (!hasTrainingCompleted) disableAuthenticationButtons();
            }
        });

        Button trainModels = findViewById(R.id.btn_start_training);
        trainModels.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_TRAIN_MODELS);

                disableAllButtons();
            }
        });

        Button startAuth = findViewById(R.id.btn_start_auth);
        startAuth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_AUTHENTICATING);

                disableAllButtons();

                Button stopAuth = findViewById(R.id.btn_stop_auth);
                stopAuth.setEnabled(true);
            }
        });

        Button stopAuth = findViewById(R.id.btn_stop_auth);
        stopAuth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendLocalBroadcast(SensingService.STATE_IDLE);

                enableAllButtons();
            }
        });
    }

    private void sendLocalBroadcast(int state) {
        Intent intent = new Intent();
        intent.setAction(SensingService.ACTION_UI);
        intent.putExtra(SensingService.EXTRA_BUTTON_STATE, state);
        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
    }

    private void enableAuthenticationButtons() {
        Button startButton = findViewById(R.id.btn_start_auth);
        Button stopButton = findViewById(R.id.btn_stop_auth);

        startButton.setEnabled(true);
        stopButton.setEnabled(true);
    }

    private void disableAuthenticationButtons() {
        Button startButton = findViewById(R.id.btn_start_auth);
        Button stopButton = findViewById(R.id.btn_stop_auth);

        startButton.setEnabled(false);
        stopButton.setEnabled(false);
    }

    private void disableAllButtons() {
        allButtons(false);
    }

    private void enableAllButtons() {
        allButtons(true);
    }

    private void allButtons(boolean value) {
        Button startUserTrain = findViewById(R.id.btn_user_start_training);
        Button stopUserTrain = findViewById(R.id.btn_user_stop_training);
        Button startImposterTrain = findViewById(R.id.btn_imposter_start_training);
        Button stopImposterTrain = findViewById(R.id.btn_imposter_stop_training);
        Button trainModels = findViewById(R.id.btn_start_training);
        Button startAuth = findViewById(R.id.btn_start_auth);
        Button stopAuth = findViewById(R.id.btn_stop_auth);

        startUserTrain.setEnabled(value);
        stopUserTrain.setEnabled(value);
        startImposterTrain.setEnabled(value);
        stopImposterTrain.setEnabled(value);
        trainModels.setEnabled(value);
        startAuth.setEnabled(value);
        stopAuth.setEnabled(value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            //process your onClick here
            startActivity(new Intent(this, PersonalDataActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
