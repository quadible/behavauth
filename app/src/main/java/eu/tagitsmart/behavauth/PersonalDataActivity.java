/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauth;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import eu.tagitsmart.behavauthlib.services.SensingService;
import eu.tagitsmart.behavauthlib.utils.PrefManager;

public class PersonalDataActivity extends AppCompatActivity {

    private static final String TAG = "PersonalDataActivity";
    private boolean areBothChecked = true;
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Personal Data");

        prefManager = new PrefManager(getApplicationContext());

        initUI();
    }

    private void initUI() {
        CheckBox locationCheckBox = findViewById(R.id.cbx_location_permission);
        locationCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked() && ((CheckBox) findViewById(R.id.cbx_bluetooth_permission)).isChecked()) {
                    areBothChecked = true;
                } else {
                    areBothChecked = false;
                }

                Intent intent = new Intent();
                intent.setAction(SensingService.ACTION_UI);
                intent.putExtra(SensingService.EXTRA_SENSING_CONSENT, areBothChecked);
                LocalBroadcastManager.getInstance(PersonalDataActivity.this).sendBroadcast(intent);
            }
        });
        boolean res = prefManager.isLocationAllowed();
        Log.i(TAG, "isLocationAllowed:" + res);
        locationCheckBox.setChecked(res);

        CheckBox bluetoothCheckBox = findViewById(R.id.cbx_bluetooth_permission);
        bluetoothCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked() && ((CheckBox) findViewById(R.id.cbx_location_permission)).isChecked()) {
                    areBothChecked = true;
                } else {
                    areBothChecked = false;
                }
                Intent intent = new Intent();
                intent.setAction(SensingService.ACTION_UI);
                intent.putExtra(SensingService.EXTRA_SENSING_CONSENT, areBothChecked);
                LocalBroadcastManager.getInstance(PersonalDataActivity.this).sendBroadcast(intent);
            }
        });
        bluetoothCheckBox.setChecked(prefManager.isBluetoothAllowed());

        Button deleteDataButton = findViewById(R.id.btn_delete_all_data);
        deleteDataButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                handleDeleteDataBtn();
            }
        });

        areBothChecked = prefManager.isBluetoothAllowed() & prefManager.isLocationAllowed();
    }

    private void handleDeleteDataBtn() {
        Log.i(TAG, "handleDeleteDataBtn");

        AlertDialog.Builder alert = new AlertDialog.Builder(
                PersonalDataActivity.this);
        alert.setTitle("Confirmation");
        alert.setMessage("Are you sure to delete all your data?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(SensingService.ACTION_UI);
                intent.putExtra(SensingService.EXTRA_DELETE_DATA, "");
                LocalBroadcastManager.getInstance(PersonalDataActivity.this).sendBroadcast(intent);

                dialog.dismiss();
            }
        });
        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }
}
