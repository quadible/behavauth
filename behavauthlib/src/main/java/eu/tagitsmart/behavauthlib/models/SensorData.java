/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.models;

import android.bluetooth.BluetoothDevice;
import android.location.Location;
import android.util.Log;

import org.servalproject.maps.utils.GeoUtils;

public class SensorData {
    private static final String TAG = "SensorData";

    private Location location;
    private BluetoothDevice bluetoothDevice;
    private double distance;
    private double speed;
    private long timestamp;

    public SensorData(BluetoothDevice device, Location location) {
        this.bluetoothDevice = device;
        this.location = location;
        this.timestamp = System.currentTimeMillis();
    }

    public SensorData(BluetoothDevice device, Location currentLocation, long currTimestamp,
                      Location previousLocation, long prevTimestamp) {
        this.bluetoothDevice = device;
        this.location = currentLocation;
        this.timestamp = currTimestamp;

        this.distance = (previousLocation == null) ? 0 : computeDistance(previousLocation);
        this.speed = (distance < 0.3) ? 0 : computeSpeed(distance, prevTimestamp);

        Log.i(TAG, "BluetoothDevice:" + device.getAddress() + ",\n" +
                "currentLocation:" + currentLocation.getLatitude() + "," + currentLocation.getLongitude() +  ",\n" +
                "currentTimestamp:" + currTimestamp +  ", " +
                "previousLocation:" + previousLocation.getLatitude() + "," + previousLocation.getLongitude() +  ",\n" +
                "previousTimestamp:" + prevTimestamp);

        Log.i(TAG, "BluetoothDevice:" + device.getAddress() + ", " +
                "location:" + location.getLatitude() + "," + location.getLongitude() + ",\n" +
                "timestamp:" + timestamp + ",\n" +
                "distance:" + distance + ",\n" +
                "speed:" + speed);
    }

    public SensorData(BluetoothDevice device, Location location, double speed, double distance) {
        this.bluetoothDevice = device;
        this.location = location;
        this.speed = speed;
        this.location = location;
        this.timestamp = System.currentTimeMillis();
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String toReadableString() {
        return "BluetoothDevice:" + bluetoothDevice.getAddress() + ", " +
                "location:" + location.getLatitude() + "," + location.getLongitude() +
                "timestamp:" + timestamp +
                "distance:" + distance +
                "speed:" + speed;
    }

    public String toString() {
        return location.getLatitude() + "," + location.getLongitude() +
                "," + bluetoothDevice.getAddress() + ","
                 + distance + "," + speed;
    }

    public String[] toStringArray() {
        return new String[] { Double.toString(location.getLatitude()),
                Double.toString(location.getLongitude()),
                bluetoothDevice.getAddress(),
                Double.toString(distance),
                Double.toString(speed)};
    }

    public void computeFeatures(Location previousLocation, long previousLocationTimestamp) {
        this.distance = computeDistance(previousLocation);
        this.speed = computeSpeed(distance, previousLocationTimestamp);
    }

    private double computeDistance(Location previousLocation) {
        return GeoUtils.calculateDistance(previousLocation.getLatitude(),
                previousLocation.getLongitude(),
                location.getLatitude(),
                location.getLongitude(),
                GeoUtils.VINCENTY_FORMULA,
                GeoUtils.METRE_UNITS);
    }

    private double computeSpeed(double distance, long previousLocationTimestamp) {
        long time = (timestamp - previousLocationTimestamp) / 3600;
        return distance/time;
    }
}
