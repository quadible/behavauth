/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.utils;

import android.util.Log;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class IODataHandlerOpenCsv implements IODataHandler {

    private static final String TAG = "IODataHandlerFastCsv";

    private File file;

    public IODataHandlerOpenCsv(String path, String filename, String[] header) {
        File folder = new File(path);
        if (!folder.isDirectory()) {
            folder.mkdirs();
        }

        this.file = new File(path, filename);

        if (!file.exists()) {
            try {
                file.createNewFile();
                writeData(header);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public IODataHandlerOpenCsv(String path, String filename) {
        File folder = new File(path);
        if (!folder.isDirectory()) {
            folder.mkdirs();
        }

        this.file = new File(path, filename);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeData(String[] line) {
        try (FileOutputStream fos = new FileOutputStream(file, true);
             OutputStreamWriter osw = new OutputStreamWriter(fos,
                     StandardCharsets.UTF_8);
             CSVWriter writer = new CSVWriter(osw)) {

            writer.writeNext(line);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "file: " + file + "could not be written");
        }
    }

    @Override
    public void deleteData() {
        Log.i(TAG, "Deleting file: " + file.getName());
        if (file != null) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public List<String> readData() {
        //TODO
        return null;
    }
}
