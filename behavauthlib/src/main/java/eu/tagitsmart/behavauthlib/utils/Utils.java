/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class Utils {

    /**
     * Request code in order to receive permissions from user
     */
    private static final int REQ_CODE = 123456;

    /**
     * App name described in the notification
     */
    private static final String APP_NAME = "TagItSmart!";

    /**
     * Context instance required to check and request permissions
     */
    private final Context context;

    public Utils (Context context) {
        this.context = context;
    }

    /**
     * Return the current state of the permissions needed.
     */
    public boolean checkLocationPermissions() {
        int permissionStateFineLocation = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionStateCoarseLocation = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return (permissionStateFineLocation == PackageManager.PERMISSION_GRANTED)
                && (permissionStateCoarseLocation == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Return the current state of the Bluetooth permissions needed.
     */
    public boolean checkProximityPermissions() {
        int permissionStateBluetooth = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.BLUETOOTH);

        int permissionStateBluetootAdmin = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.BLUETOOTH_ADMIN);

        int permissionStateCoarseLocation = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return (permissionStateBluetooth == PackageManager.PERMISSION_GRANTED) &&
                (permissionStateBluetootAdmin == PackageManager.PERMISSION_GRANTED) &&
                (permissionStateCoarseLocation == PackageManager.PERMISSION_GRANTED);
    }

}