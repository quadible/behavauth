/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.ml;

import android.util.Log;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;

import eu.tagitsmart.behavauthlib.models.SensorData;
import weka.classifiers.Classifier;
import weka.classifiers.trees.DecisionStump;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

public class MachineLearningHandler {

    private static final String TAG = "MachineLearningHandler";

    private String path;
    private String filename;
    private String classfierFilename;
    private ArrayList<Attribute> attributeList;
    private Classifier classifier;


    public MachineLearningHandler(String path, String trainDataFilename, String classfierFilename) {
        this.path = path;
        this.filename = trainDataFilename;
        this.classfierFilename = classfierFilename;

        init();
    }

    private void init() {
        attributeList = new ArrayList<Attribute>(5);

        Attribute latitude = new Attribute("Latitude");
        Attribute longitude = new Attribute("Longitude");
        Attribute macAddress = new Attribute("MAC Address");
        Attribute distance = new Attribute("Distance");
        Attribute speed = new Attribute("Speed");
        Attribute classAttr = new Attribute("Class");

        ArrayList<String> classVal = new ArrayList<String>();
        classVal.add("user");
        classVal.add("imposter");

        attributeList.add(latitude);
        attributeList.add(longitude);
        attributeList.add(macAddress);
        attributeList.add(distance);
        attributeList.add(speed);
        attributeList.add(new Attribute("@@class@@", classVal));
    }

    public void trainModel() throws Exception {
        CSVLoader loader = new CSVLoader();
        loader.setSource(new File(path, filename));

        Instances trainData = loader.getDataSet();

        trainData.setClassIndex(trainData.numAttributes() - 1);

        classifier = new DecisionStump();
        classifier.buildClassifier(trainData);

        weka.core.SerializationHelper.write(path + File.separator + classfierFilename, classifier);
    }

    public double classify(SensorData sensorData) throws Exception {
        if (classifier == null) {
            classifier = (Classifier) weka.core.SerializationHelper.read(path + File.separator + classfierFilename);
        }

        // Create attributes to be used with classifiers
        double result = -1;
        try {
            Instances instances = new Instances("TestInstances",attributeList,0);

            double[] instanceValues = new double[instances.numAttributes()];
            instanceValues[0] = sensorData.getLocation().getLatitude();
            instanceValues[1] = sensorData.getLocation().getLongitude();
            instanceValues[2] = instances.attribute(2).addStringValue(sensorData.getBluetoothDevice().getAddress());
            instanceValues[3] = sensorData.getDistance();
            instanceValues[4] = sensorData.getSpeed();

            instances.add(new DenseInstance(1.0, instanceValues));
            instances.setClassIndex(instances.numAttributes() - 1);

            result = classifier.classifyInstance(instances.instance(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void deleteModels() {
        File trainFile = new File(path, filename);
        if (trainFile.exists()) {
            Log.i(TAG, "Delete file:" + filename);
            trainFile.delete();
        }

        File classifierFile = new File(path, classfierFilename);
        if (classifierFile.exists()) {
            Log.i(TAG, "Deleting file:" + classfierFilename);
            classifierFile.delete();
        }
    }
}
