/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.ml;

import android.os.Environment;
import android.util.Log;

import java.io.File;

import eu.tagitsmart.behavauthlib.models.AuthData;
import eu.tagitsmart.behavauthlib.models.SensorData;
import eu.tagitsmart.behavauthlib.utils.IODataHandler;
import eu.tagitsmart.behavauthlib.utils.IODataHandlerOpenCsv;

public class Authenticator {

    private static final String IMPOSTER = "imposter";
    private static final String TAG = "Authenticator";
    private static final boolean D = true;

    private static final String FILENAME_CLASSIFIER = "classifier.model";
    private static final String FILENAME = "training_data.csv";
    private static final String DIRECTORY = Environment.getExternalStorageDirectory() + File.separator + "BehavAuth";
    private static final String[] HEADER = new String[] {"Latitude", "Longitude", "MAC Address", "Distance", "Speed", "Class"};

    private String name;
    private AuthData authData;
    private AuthenticatorCallback callback;
    private IODataHandler dataHandler;
    private MachineLearningHandler machineLearningHandler;

    public Authenticator (AuthenticatorCallback callback, AuthData authData) {
        this.callback = callback;
        this.authData = authData;
        this.name = authData.getUpdateUser();
        this.dataHandler = new IODataHandlerOpenCsv(DIRECTORY, FILENAME, HEADER);
        this.machineLearningHandler = new MachineLearningHandler(DIRECTORY, FILENAME, FILENAME_CLASSIFIER);
    }

    public void addUserData(SensorData data) {
        addData(data, "user");
    }

    public void addImposterData(SensorData data) {
        addData(data, "imposter");
    }

    private void addData(SensorData data, String user) {
        if (D) Log.i(TAG, "addData(): " + data.toString() + "," + user);

        String[] sensorData = data.toStringArray();
        String[] userData = new String[] { user };

        String[] allData = new String[sensorData.length + userData.length];
        System.arraycopy(sensorData, 0, allData,0, sensorData.length);
        allData[allData.length - 1] = user;

        dataHandler.writeData(allData);
    }

    public void trainModel() {
        if (D) Log.i(TAG, "trainModel()");
        try {
            machineLearningHandler.trainModel();
            // Inform the service that the model training was finished
            callback.onTrainingFinished();
        } catch (Exception e) {
            e.printStackTrace();

            callback.onTrainingFailed();
        }
    }

    public void classify(SensorData data) {
        if (D) Log.i(TAG, "classify(): " + data.toString());

        AuthData result;
        try {
            if (machineLearningHandler.classify(data) > 0) {
                authData.setUpdateUser(IMPOSTER);
            } else {
                authData.setUpdateUser(name);
            }

            result = authData;
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }

        // Inform the service about the authentication result
        callback.onAuthenticationFinished(result);
    }

    public void deleteData() {
        dataHandler.deleteData();
        machineLearningHandler.deleteModels();
    }
}
