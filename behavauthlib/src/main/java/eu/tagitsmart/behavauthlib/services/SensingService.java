/*
 * Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
 * funded by European Union's Horizon 2020 Research and Innovation Programme.
 *
 * This file is part of the BehavAuth mobile app
 *
 * BehavAuth mobile app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package eu.tagitsmart.behavauthlib.services;

import android.app.Service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;

import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import eu.tagitsmart.behavauthlib.activities.PermissionsActivity;
import eu.tagitsmart.behavauthlib.ml.Authenticator;
import eu.tagitsmart.behavauthlib.ml.AuthenticatorCallback;
import eu.tagitsmart.behavauthlib.models.SensorData;
import eu.tagitsmart.behavauthlib.utils.Utils;
import eu.tagitsmart.behavauthlib.models.AuthData;

public class SensingService extends Service {

    private static final String TAG = SensingService.class.getSimpleName();

    /**
     * Code used when sending messages to the Parent Activity
     */
    public static final String INTENT_DATA_SERVICE = "eu.tagitsmart.behavauthlib.SensingService.INTENT_DATA_SERVICE";

    /**
     * Code used when receiving messages from PermissionsActivity
     */
    public static final String INTENT_PERMISSIONS_SET = "eu.tagitsmart.behavauthlib.SensingService.INTENT_PERMISSIONS_SET";

    /**
     * Event name to transfer the variable if the location is enabled
     */
    public static final String LOCATION_ENABLED = "location-enabled";

    /**
     * Event name to transfer the variable if the bluetooth is enabled
     */
    public static final String BLUETOOTH_ENABLED = "bluetooth-enabled";

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_LOCATION_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The desired interval for Bluetooth proximity updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_BLUETOOTH_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_LOCATION_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    private static final String TIS_INTENT_FILTER = "eu.tagitsmart.auth.pollingmanager";
    private static final String AUTH_DATA_DEVICEID = "deviceID";
    private static final String AUTH_DATA_EMAIL = "email";
    private static final String AUTH_DATA_PASSWORD = "password";
    private static final String AUTH_DATA_UPDATEUSER = "updateUser";
    private static final String AUTH_AUTHENTICATOR_ID = "authenticatorId";
    private static final String AUTHENTICATOR_ID = "behavAuth";

    public static final String EXTRA_TRAINING_FINISHED = "extra-training-finished";
    public static final String ACTION_UI = "eu.tagitsmart.behavauthlib.services.SensingService.ACTION_UI";
    public static final String EXTRA_BUTTON_STATE = "button-state";
    public static final String EXTRA_DELETE_DATA = "delete-data";
    public static final String EXTRA_SENSING_CONSENT = "sensing-consent";

    /**
     * Provides access to the Bluetooth Adapter API
     */
    private volatile BluetoothAdapter mBluetoothAdapter;

    /**
     * Allows setting the delay of the Bluetooth discovery
     */
    private Handler mBluetoothDiscoveryHandler;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Provides access to the Location Settings API.
     */
    private SettingsClient mSettingsClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Callback for Location events.
     */
    private LocationCallback mLocationCallback;

    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;

    private long mCurrentLocationTimestamp;

    /**
     * Represent the previous location
     */
    private Location mPreviousLocation;

    private long mPreviousLocationTimestamp;

    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;

    /**
     * Utilities enabler to handler permissions
     */
    private Utils mUtils;


    private List<BluetoothDevice> mProximityBluetoothDevices;

    private Authenticator mAuthenticator;
    private AuthenticatorCallback mAuthenticatorCallback;
    private AuthData mCurrentUserAuthData;

    private Queue<SensorData> mSensorData;

    private Handler mInferenceHandler;

    public static final int STATE_IDLE = 0;
    public static final int STATE_USER_TRAINING = 1;
    public static final int STATE_IMPOSTER_TRAINING = 2;
    public static final int STATE_AUTHENTICATING = 3;
    public static final int STATE_TRAIN_MODELS = 4;

    private static int mState = STATE_IDLE;

    /**
     * Broadcast receiver to communicate with other activities
     */
    //private DataUpdateReceiver mDataReceiver;


    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");

        mUtils = new Utils(this);

        mLastUpdateTime = "";

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothDiscoveryHandler = new Handler();
        mProximityBluetoothDevices = new ArrayList<>();

        setupAuthData();

        mAuthenticatorCallback = new TISAuthenticatorCallback();
        mAuthenticator = new Authenticator(mAuthenticatorCallback, mCurrentUserAuthData);

        mSensorData = new ConcurrentLinkedQueue<>();

        mInferenceHandler = new Handler();
        mInferenceHandler.post(new InferenceRunnable());

        mState = STATE_IDLE;

        //mDataReceiver = new DataUpdateReceiver();

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    private void setupAuthData() {
        mCurrentUserAuthData = new AuthData();
        mCurrentUserAuthData.setDeviceId("XX:XX:XX:XX:XX:XX");
        mCurrentUserAuthData.setEmail("this@ismyemail.com");
        mCurrentUserAuthData.setPassword("test1234");
        mCurrentUserAuthData.setUpdateUser("username");
    }

    private void requestPermissionsWizard() {
        Intent intent = new Intent();
        intent.setClass(this, PermissionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");

        // Register broadcast listener to receive messages from activities
        //if (mDataReceiver == null) mDataReceiver = new DataUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(SensingService.INTENT_PERMISSIONS_SET);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDataReceiver, intentFilter);
        IntentFilter uiIntentFilter = new IntentFilter(SensingService.ACTION_UI);
        LocalBroadcastManager.getInstance(this).registerReceiver(mUIDataReceiver, uiIntentFilter);

        // Request all permissions required by the app
        requestPermissionsWizard();

        // Initiates both location and bluetooth sensing based on permissions
        startSensing();

        //TODO
        updateUI();

        return START_STICKY;
    }

    private void startSensing() {
        // Within {@code onDestroy()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.
        if (mUtils.checkLocationPermissions()) {
            startLocationUpdates();
        } else {
            Log.i(TAG, "Location permissions are not set.");
        }

        // Within {@code onDestroy()}, we remove Bluetooth proximity updates. Here, we resume
        // Bluetooth proximity updates if the user has requested them.
        if (mUtils.checkProximityPermissions()) {

            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            }

            registerReceiver(mBluetoothBroadcastReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));

            // Start the Bluetooth discovery process
            mBluetoothDiscoveryHandler.post(new BluetoothDiscoveryRunnable());
        } else {
            Log.i(TAG, "Bluetooth permissions are not set.");
        }
    }

    private void stopSensing() {

        // Stop location updates
        stopLocationUpdates();

        // Stop bluetooth discovery
        mBluetoothDiscoveryHandler.removeCallbacks(null);

        try {
            unregisterReceiver(mBluetoothBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            //e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister broadcast listener to stop receiving messages from service.
        if (mDataReceiver != null) LocalBroadcastManager.getInstance(this).unregisterReceiver(mDataReceiver);

        if (mUIDataReceiver != null) LocalBroadcastManager.getInstance(this).unregisterReceiver(mUIDataReceiver);

        // Stop scanning for Bluetooth devices
        if (mBluetoothBroadcastReceiver != null) unregisterReceiver(mBluetoothBroadcastReceiver);

        // Stop the location update once the service is destroyed
        stopLocationUpdates();
    }


    /**
     * Performs Bluetooth discovery to detect any nearby bluetooth devices
     * and triggering the Bluetooth broadcast receiver.
     */
    private void startBluetoothProximityUpdates() {

        // Stop any previous Bluetooth discovery process
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        // Start the discovery process
        mBluetoothAdapter.startDiscovery();
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        //TODO
                        updateUI();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
//                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
//                                    ResolvableApiException rae = (ResolvableApiException) e;
//                                    rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
//                                } catch (IntentSender.SendIntentException sie) {
//                                     Log.i(TAG, "PendingIntent unable to execute request.");
//                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                //Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                //mRequestingLocationUpdates = false;
                        }

                        updateUI();
                    }
                });
    }


    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //mRequestingLocationUpdates = false;

                        //TODO
                        // setButtonsEnabledState();
                    }
                });
    }


    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_LOCATION_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if (mPreviousLocation != null) {
                    mPreviousLocation = mCurrentLocation;
                    mPreviousLocationTimestamp = mCurrentLocationTimestamp;
                }

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                mCurrentLocationTimestamp = System.currentTimeMillis();

                if (mPreviousLocation == null) {
                    mPreviousLocation = mCurrentLocation;
                    mPreviousLocationTimestamp = mCurrentLocationTimestamp;
                }

                //TODO
                updateUI();
            }
        };
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        if (mCurrentLocation != null && mLastUpdateTime != null)
            Log.i(TAG, String.format(Locale.ENGLISH, "%s: %f, %f at %s", "Location", mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), mLastUpdateTime));
        else
            Log.i(TAG, String.format(Locale.ENGLISH, "%s: %s at %s", "Location", ((mCurrentLocation == null) ? "NULL" : mCurrentLocation.toString()), ((mLastUpdateTime == null) ? "NULL" : mLastUpdateTime) ));
    }

    private void sendAuthenticationResultToPollingManager(AuthData authData) {
        Intent intent = new Intent();
        intent.setAction(TIS_INTENT_FILTER);
        intent.putExtra(AUTH_DATA_DEVICEID, authData.getDeviceId());
        intent.putExtra(AUTH_DATA_EMAIL,authData.getEmail());
        intent.putExtra(AUTH_DATA_PASSWORD,authData.getPassword());
        intent.putExtra(AUTH_DATA_UPDATEUSER,authData.getUpdateUser());
        intent.putExtra(AUTH_AUTHENTICATOR_ID, AUTHENTICATOR_ID);
        sendBroadcast(intent);
    }


    /**
     * Receives all Bluetooth messages and nearby devices.
     */
    private final BroadcastReceiver mBluetoothBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (mCurrentLocation != null && mPreviousLocation != null) {
                    // Store Bluetooth device and last known location
                    mSensorData.add(new SensorData(device, mCurrentLocation,
                            mCurrentLocationTimestamp, mPreviousLocation, mPreviousLocationTimestamp));
                }
            }
        }
    };

    /**
     * Runnable that implements the periodic initiation of Bluetooth scanning,
     * every UPDATE_BLUETOOTH_INTERVAL_IN_MILLISECONDS milliseconds.
     */
    private class BluetoothDiscoveryRunnable implements Runnable {

        @Override
        public void run() {
            // Remove previously discovered Bluetooth devices
            mProximityBluetoothDevices.clear();

            // Start discovering any nearby Bluetooth devices
            startBluetoothProximityUpdates();

            // Restart the discovery after UPDATE_BLUETOOTH_INTERVAL_IN_MILLISECONDS
            mBluetoothDiscoveryHandler.postDelayed(this, UPDATE_BLUETOOTH_INTERVAL_IN_MILLISECONDS);
        }
    }

    /**
     * This broadcast receiver is used to receive messages Activities
     */
    private BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "DataUpdateReceiver:onReceive()" + intent.toString());

            if (intent.getAction().equals(SensingService.INTENT_PERMISSIONS_SET)) {

                startSensing();
            }
        }
    };

    private BroadcastReceiver mUIDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.contains(ACTION_UI)) {

                if (intent.hasExtra(EXTRA_BUTTON_STATE)) {
                    int state = intent.getIntExtra(EXTRA_BUTTON_STATE, -1);
                    if (state <= STATE_IDLE) {
                        mState = STATE_IDLE;
                    } else {
                        mState = state;
                    }

                    if (mState == STATE_TRAIN_MODELS) {
                        mAuthenticator.trainModel();
                    }
                } else if (intent.hasExtra(EXTRA_SENSING_CONSENT)) {
                    Log.i(TAG, "EXTRA_CONSENT: " + intent.getBooleanExtra(EXTRA_SENSING_CONSENT, false));
                    boolean consent = intent.getBooleanExtra(EXTRA_SENSING_CONSENT, false);
                    if (consent) {
                        startSensing();
                    } else {
                        stopSensing();
                    }
                } else if (intent.hasExtra(EXTRA_DELETE_DATA)) {
                    mAuthenticator.deleteData();
                }
            }
        }
    };

    private class TISAuthenticatorCallback implements AuthenticatorCallback {

        @Override
        public void onTrainingFinished() {
            Intent intent = new Intent();
            intent.setAction(INTENT_DATA_SERVICE);
            intent.putExtra(EXTRA_TRAINING_FINISHED, true);
            sendBroadcast(intent);
        }

        @Override
        public void onTrainingFailed() {
            Intent intent = new Intent();
            intent.setAction(INTENT_DATA_SERVICE);
            intent.putExtra(EXTRA_TRAINING_FINISHED, false);
            sendBroadcast(intent);
        }

        @Override
        public void onAuthenticationFinished(AuthData data) {
            Log.i(TAG, data.toString());
            sendAuthenticationResultToPollingManager(data);
        }
    }

    private long NORMAL_INFERENCE_PERIOD = 1000;
    private long INCREASE_STEP_FOR_INFERENCE_PERIOD = 1000;
    private long inferenceDelayPeriod = NORMAL_INFERENCE_PERIOD;

    /**
     * This is the maximum delay that the app waits
     * to check for a new BT sample.
     */
    private static final long MAX_DELAY_PERIOD = 10000;
    private static final boolean D = true;

    private class InferenceRunnable implements Runnable {
        private static final String TAG = "InferenceRunnable";

        @Override
        public void run() {
            if (D) Log.i(TAG, "run()" );

            // Adapt the inference period
            if (mSensorData.isEmpty() && inferenceDelayPeriod < MAX_DELAY_PERIOD) {
                inferenceDelayPeriod += INCREASE_STEP_FOR_INFERENCE_PERIOD;
            } else {
                // While there are sensor data in the queue
                while (true) {
                    if (mSensorData.isEmpty())
                        break;
                    
                    // Reinitialise the period
                    inferenceDelayPeriod = NORMAL_INFERENCE_PERIOD;
                    // Get the oldest sample
                    SensorData data = mSensorData.poll();
                    // Compute distance and speed
                    //data.computeFeatures(mPreviousLocation, mPreviousLocationTimestamp);

                    switch (mState) {

                        case STATE_AUTHENTICATING:
                            mAuthenticator.classify(data);
                            break;

                        case STATE_USER_TRAINING:
                            mAuthenticator.addUserData(data);
                            break;

                        case STATE_IMPOSTER_TRAINING:
                            mAuthenticator.addImposterData(data);
                            break;

                        case STATE_TRAIN_MODELS:
                        case STATE_IDLE:
                        default:
                            break;

                    }
                }
            }

            // Reschedule the inference
            mInferenceHandler.postDelayed(this, inferenceDelayPeriod);
        }
    }
}