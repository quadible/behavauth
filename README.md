# BehavAuth

BehavAuth will replace old-fashioned password based authentication with autonomous multi-biometric authentication by learning user�s behavioural patterns.

This section describes the BehavAuth mobile app, which is a mobile app that learns the behaviour of the user and authenticates them to the TagItSmart platform.

The BehavAuth app operates mainly in an opportunistic manner. This means that the users just need to install the mobile app. After the installation process, the mobile app operates on its own without the needs of the users input.

---

## Installation

Follow the instructions below to install the app.

1. Download the .apk from [here]() .
2. Click on the download notification to start installation.

---

## Permissions

These are the permissions required for the app.

1. 	Have access to the location sensor, to estimate the location, speed and distance patterns.

```
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```

2.  Have access to the Bluetooth interface, to estimate the social interaction patterns of the users
```
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
```

3. Have access to write to the SD card, in order to store the training data and also the machine-learning models
```
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

```

---

## SDK Requirements

The requirements for the Android SDK are provided below:

```
android {
    compileSdkVersion 27
    buildToolsVersion "27.0.3"
    defaultConfig {
        applicationId "eu.tagitsmart.behavauth"
        minSdkVersion 21
        targetSdkVersion 27
        versionCode 1
        versionName "1.0"       
    }
}

```

---

## Acknowledgements
The **BehavAuth** mobile app is part of the [TagItSmart EU project (688061)](www.tagitsmart.eu) that was funded by European Union's Horizon 2020 Research and Innovation Programme.

---

## License

```
Copyright (C) 2018 BehavAuth Project part of TagItSmart EU Project (688061),
funded by European Union's Horizon 2020 Research and Innovation Programme.

This file is part of the BehavAuth mobile app

BehavAuth mobile app is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This source code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this source code; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
```